import pytest
import subprocess
import re

from pathlib import Path

# base_dir = '/hsm/testing/prepared'
base_dir = r'/hsm/k204221/testing/testing/prepared01'

test_cases = [
      [
        [ "./slk.sh", "tag", 'document.author="Caesar Test 01"', base_dir+"/tag/file_set_author/file_01.txt"],
        "", "", 0, "tag test 01: for file set document.author", '"{\"document.Author\": "Ceaser Test 01"}"', base_dir+"/tag/file_set_author"
      ],
]


# FIXME: I don't check any owner right.. it shouldn't be ok, that I am allowed to change Daniels Files at all..
class TestTag:

    @pytest.mark.parametrize("slk_command,expected,stderr,returncode,comment,permissions,dir", test_cases)

    def test_tag(self, slk_command, expected, stderr, returncode, comment, tags, dir):
        output = subprocess.run(slk_command, capture_output=True, encoding="ascii")
        assert output.returncode == returncode, comment+'; bad exit code: '+str(output.returncode)
        if (output.returncode == 0 and expected):
          assert output.stdout == expected, comment+'; bad stdout'
          
        if (stderr):
          assert output.stderr == stderr, comment+'; bad stderr'

        if (returncode == 0):
            output_list = subprocess.run(['./slk.sh', 'list', dir], capture_output=True, encoding="ascii")
            # check whether slk list ran properly
            assert output_list.returncode == 0, comment+'; slk list bad output'
            ## simple:
            # assert (permissions in output_list.stdout.split('\n')[1])+'; bad permissions: '
            ## extended:
            # check whether correct output of slk list
            assert len(output_list.stdout.split('\n')) == 4, comment+'; slk list too many files listed'
            # create regex to find permissions and apply it on second line of slk list output
            pattern = re.compile("[r-][w-][x-][r-][w-][x-][r-][w-][x-]")
            result = re.search(pattern, output_list.stdout.split('\n')[1])
            # did not find permission pattern in string => something wrong
            assert result is not None, comment+'; slk list permissions not found'
            # extract permissions and check against provided permissions
            assert result.group(0) == permissions, comment+'; wrong permissions set'
