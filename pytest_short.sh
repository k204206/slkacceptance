#!/bin/bash

# pytest wrapper for short output

exec pytest --tb=no "$@"
