import pytest
import subprocess
import re
import warnings
from pathlib import Path


base_folder = "/hsm/k204206/move_tests/"
test_folder_permissions_ignored = base_folder + "permissions/folder_rwxrwxrwx/"
test_folder_no_write_permission = base_folder + "permissions/folder_r_xr_xr_x/"

# tests mit permission

test_cases_should_fail = [
    pytest.param(
        ["slk", "move", "/does/not/exist", test_folder_permissions_ignored], "ERROR: No resource or namespace to move found at path \"/does/not/exist\". Add a preceding slash if necessary.", 1, id="move from non existing path"
    ),
    pytest.param(
        ["slk", "move", test_folder_permissions_ignored + "folder1", base_folder+"doesnotexist"], "ERROR: No destination namespace found at path \"" + base_folder + "doesnotexist\". Add a preceding slash if necessary.", 1, id="move to non existing path"
    ),
    pytest.param(
        ["slk", "move", test_folder_permissions_ignored + "folder2", test_folder_permissions_ignored + "folder2/subfolder"], "ERROR: Unable to move resource.", 1, id="move folder into own subdir"
    ),
    pytest.param(
        ["slk", "move", test_folder_permissions_ignored + "file4", test_folder_no_write_permission], "ERROR: Unable to move resource. User must have write permission for source and destination namespaces.", 1, id="move file to folder without write permission"
    ),
    pytest.param(
        ["slk", "move", test_folder_permissions_ignored + "file1", test_folder_permissions_ignored], "ERROR: Attempting to move resource to its existing location. No change. Please choose a new destination namespace.", 1, id="move file to own dir (NO-OP)"
    ),
    pytest.param(
        ["slk", "move", test_folder_no_write_permission + "file_rwx", test_folder_permissions_ignored], "ERROR: Unable to move resource. User must have write permission for source and destination namespaces.", 1, id="move writable file from folder without write permission to folder with write permission"
    ),
  ]


test_cases_should_succeed = [
    pytest.param(
        ["slk", "move", test_folder_permissions_ignored + "folder3", str(Path(test_folder_permissions_ignored).parent.absolute())], 0, id="move folder to own parent dir (NO-OP)"
    ),
    pytest.param(
        ["slk", "move", test_folder_permissions_ignored + "file2", test_folder_permissions_ignored + "fixfolder/"], 0, id="move file into subdir (and pending /)"
    ),
    pytest.param(
        ["slk", "move", test_folder_permissions_ignored + "fixfolder/file3", test_folder_permissions_ignored], 0, id="move file into parent dir"
    ),
    pytest.param(
        ["slk", "move", test_folder_permissions_ignored + "fixfolder/subfolder1", test_folder_permissions_ignored], 0, id="move folder one level down"
    ),
    pytest.param(
        ["slk", "move", test_folder_permissions_ignored + "file1_r_x", test_folder_permissions_ignored + "fixfolder" ], 0, id="move non-writable-file from writable dir to writable subdir"
    ),
    pytest.param(
        ["slk", "move", (test_folder_no_write_permission + "subfolder_rwx/file5"), test_folder_permissions_ignored + "fixfolder" ], 0, id="move writable-file from writable subdir (in non-writable dir) to writable dir"
    ),
  ]


# move complicated cases: Move file to parent dir where file with this name already exists...
# move complicated cases: Move file to sub dir where file with this name already exists...


class TestMove:
    @pytest.mark.parametrize("slk_command, expected_output, return_code", test_cases_should_fail)
    def test_should_fail(self, slk_command, expected_output, return_code):
        output = subprocess.run(slk_command, capture_output=True, encoding="ascii")
        assert output.returncode == return_code, "Wrong return code"
        if (not output.stderr and output.stdout):
          warnings.warn(UserWarning( "Output on stderr expected, but got on stdout"))
          assert expected_output == output.stdout.strip(), "Wrong output: " + output.stdout
        elif (not output.stderr):
          assert output.stderr, "Output on stderr expected"
        else:
          assert expected_output == output.stderr.strip(), "Wrong output: " + output.stderr

    @pytest.mark.parametrize("slk_command, return_code", test_cases_should_succeed)
    def test_should_succeed(self, slk_command, return_code):
        print (slk_command)
        output = subprocess.run(slk_command, capture_output=True, encoding="ascii")
        print(output.args)
        assert output.returncode == return_code, "Wrong return code"
        assert not output.stdout and not output.stderr, "No output expected"
