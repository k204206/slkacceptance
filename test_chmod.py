import pytest
import subprocess
import re

from pathlib import Path

# base_dir = '/hsm/testing/prepared'
base_dir = r'/hsm/k204221/testing/testing/prepared08/pre_slk'

test_cases = [
      pytest.param(
        [ "./slk.sh", "chmod", "770", base_dir+"/slk_chmod/files_a/set_file_770/file_01.txt"],
        "", "", 0,'rwxrwx---', base_dir+"/slk_chmod/files_a/set_file_770", id="chmod test 01: set permissions to rwxrwx---"
      ),
      pytest.param(
        [ "./slk.sh", "chmod", "771", base_dir+"/slk_chmod/files_a/set_file_771/file_01.txt"],
        "", "", 0,'rwxrwx--x', base_dir+"/slk_chmod/files_a/set_file_771", id="chmod test 02: set permissions to rwxrwx--x"
      ),
      pytest.param(
        [ "./slk.sh", "chmod", "772", base_dir+"/slk_chmod/files_a/set_file_772/file_01.txt"],
        "", "", 0,'rwxrwx-w-', base_dir+"/slk_chmod/files_a/set_file_772", id="chmod test 03: set permissions to rwxrwx-w-"
      ),
      pytest.param(
        [ "./slk.sh", "chmod", "773", base_dir+"/slk_chmod/files_a/set_file_773/file_01.txt"],
        "", "", 0,'rwxrwx-wx', base_dir+"/slk_chmod/files_a/set_file_773", id="chmod test 04: set permissions to rwxrwx-wx"
      ),
      pytest.param(
        [ "./slk.sh", "chmod", "774", base_dir+"/slk_chmod/files_a/set_file_774/file_01.txt"],
        "", "", 0,'rwxrwxr--', base_dir+"/slk_chmod/files_a/set_file_774", id="chmod test 05: set permissions to rwxrwxr--"
      ),
      pytest.param(
        [ "./slk.sh", "chmod", "775", base_dir+"/slk_chmod/files_a/set_file_775/file_01.txt"],
        "", "", 0,'rwxrwxr-x', base_dir+"/slk_chmod/files_a/set_file_775", id="chmod test 06: set permissions to rwxrwxr-x"
      ),
      pytest.param(
        [ "./slk.sh", "chmod", "776", base_dir+"/slk_chmod/files_a/set_file_776/file_01.txt"],
        "", "", 0,'rwxrwxrw-', base_dir+"/slk_chmod/files_a/set_file_776", id="chmod test 07: set permissions to rwxrwxrw-"
      ),
      pytest.param(
        [ "./slk.sh", "chmod", "777", base_dir+"/slk_chmod/files_a/set_file_777/file_01.txt"],
        "", "", 0,'rwxrwxrwx', base_dir+"/slk_chmod/files_a/set_file_777", id="chmod test 08: set permissions to rwxrwxrwx"
      ),
      pytest.param(
        [ "./slk.sh", "chmod", "755", base_dir+"/slk_chmod/files_a/set_file_755/file_01.txt"],
        "", "", 0,'rwxr-xr-x', base_dir+"/slk_chmod/files_a/set_file_755", id="chmod test 09: set permissions to rwxr-xr-x"
      ),
      pytest.param(
        [ "./slk.sh", "chmod", "666", base_dir+"/slk_chmod/files_a/set_file_666/file_01.txt"],
        "", "", 0,'rw-rw-rw-', base_dir+"/slk_chmod/files_a/set_file_666", id="chmod test 10: set permissions to rw-rw-rw-"
      ),
      pytest.param(
        [ "./slk.sh", "chmod", "640", base_dir+"/slk_chmod/files_a/set_file_640/file_01.txt"],
        "", "", 0,'rw-r-----', base_dir+"/slk_chmod/files_a/set_file_640", id="chmod test 11: set permissions to rw-r-----"
      ),
      pytest.param(
        [ "./slk.sh", "chmod", "o-r", base_dir+"/slk_chmod/files_a/set_file_o-r/file_01.txt"],
        "", "", 0,'rwxrwx-wx', base_dir+"/slk_chmod/files_a/set_file_o-r", id="chmod test 12: set permissions to rwxrwx-wx"
      ),
      pytest.param(
        [ "./slk.sh", "chmod", "go-w", base_dir+"/slk_chmod/files_a/set_file_go-w/file_01.txt"],
        "", "", 0,'rwxr-xr-x', base_dir+"/slk_chmod/files_a/set_file_go-w", id="chmod test 13: set permissions to rwxr-xr-x"
      ),
      pytest.param(
        [ "./slk.sh", "chmod", "u-x", base_dir+"/slk_chmod/files_a/set_file_u-x/file_01.txt"],
        "", "", 0,'rw-rwxrwx', base_dir+"/slk_chmod/files_a/set_file_u-x", id="chmod test 14: set permissions to rw-rwxrwx"
      ),
      pytest.param(
        [ "./slk.sh", "chmod", "644", base_dir+r"/slk_chmod/files_a/set_file_644_special_char/file_$.txt"],
        "", "", 0,'rw-r--r--', base_dir+"/slk_chmod/files_a/set_file_644_special_char", id="chmod test 15: set permissions of file with special character to rw-r--r--"
      ),
      pytest.param(
        [ "./slk.sh", "chmod", "644", base_dir+"/slk_chmod/set_folder_644_keep_files"],
        "", "", 0,'rwxrwxrwx', base_dir+"/slk_chmod/set_folder_644_keep_files", id="chmod test 16: set permissions of folder to rw-r--r-- (no -R) but keep file with rwxrwxrwx"
      ),
      pytest.param(
        [ "./slk.sh", "chmod", "-R", "644", base_dir+"/slk_chmod/set_folder_and_files_644"],
        "", "", 0,'rw-r--r--', base_dir+"/slk_chmod/set_folder_and_files_644", id="chmod test 17: set permissions of folder (and content) to rw-r--r-- (with -R)"
      ),
      pytest.param(
        [ "./slk.sh", "chmod", "-R", "644", base_dir+"/slk_chmod/set_folder_and_files_644/"],
        "", "", 0,'rw-r--r--', base_dir+"/slk_chmod/set_folder_and_files_644", id="chmod test 18: set permissions of folder with trailing slash (and content) to rw-r--r-- (with -R)"
      ),
      pytest.param(
        [ "./slk.sh", "chmod", "644", base_dir+"/slk_chmod/file_not_exists.txt"],
        "", None, 1, '', '', id="chmod test 19: set permissions of non-existing file to rw-r--r--"
      ),
      pytest.param(
        [ "./slk.sh", "chmod", "644", base_dir+"/slk_chmod/folder_not_exists/"],
        "", None, 1, '', '', id="chmod test 20: set permissions of non-existing folder to rw-r--r-- (without -R)"
      ),
      pytest.param(
        [ "./slk.sh", "chmod", "-R", "644", base_dir+"/slk_chmod/folder_not_exists/"],
        "", None, 1, '', '', id="chmod test 21: set permissions of non-existing folder to rw-r--r-- (with -R)"
      ),
      pytest.param(
        [ "./slk.sh", "chmod", "644", base_dir+"/slk_chmod/check_permissions_file_no_write_a/file_01.txt"],
        "", None, 1, '', '', id="chmod test 22: set permissions of file without write permissions to rw-r--r--"
        # todo: error message
      ),
      pytest.param(
        [ "./slk.sh", "chmod", "644", base_dir+"/slk_chmod/check_permissions_file_no_read_a/file_01.txt"],
        "", "", 0, 'rw-r--r--', base_dir+"/slk_chmod/check_permissions_file_no_read_a", id="chmod test 23: set permissions of file without write permissions to rw-r--r--"
      ),
      pytest.param(
        [ "./slk.sh", "chmod", "-R", "644", base_dir+"/slk_chmod/check_permissions_file_no_write_b/"],
        "", None, 1, '', '', id="chmod test 24: set permissions recursively of folder with file without write permissions to rw-r--r--; with -R"
        # todo: error message
      ),
      pytest.param(
        [ "./slk.sh", "chmod", "-R", "644", base_dir+"/slk_chmod/check_permissions_file_no_read_b/"],
        "", "", 0, 'rw-r--r--', base_dir+"/slk_chmod/check_permissions_file_no_read_b", id="chmod test 25: set permissions recursively of folder with file without write permissions to rw-r--r--; with -R"
      ),
      pytest.param(
        [ "./slk.sh", "chmod", "644", base_dir+"/slk_chmod/folder_no_read/file_01.txt"],
        "", "", 0, 'rw-r--r--', base_dir+"/slk_chmod/folder_no_read", id="chmod test 26: set permissions of file in folder without read permissions to rw-r--r--"
      )
]


# FIXME: I don't check any owner right.. it shouldn't be ok, that I am allowed to change Daniels Files at all..
class TestChmod:

    @pytest.mark.parametrize("slk_command,expected,stderr,returncode,permissions,dir", test_cases)

    def test_chmod(self, slk_command, expected, stderr, returncode, permissions, dir):
        output = subprocess.run(slk_command, capture_output=True, encoding="ascii")
        assert output.returncode == returncode
        if (output.returncode == 0 and expected):
          assert output.stdout == expected
          
        if (stderr):
          assert output.stderr == stderr

        if (returncode == 0):
            output_list = subprocess.run(['./slk.sh', 'list', dir], capture_output=True, encoding="ascii")
            # check whether slk list ran properly
            assert output_list.returncode == 0
            ## simple:
            # assert (permissions in output_list.stdout.split('\n')[1])+'; bad permissions: '
            ## extended:
            # check whether correct output of slk list
            assert len(output_list.stdout.split('\n')) == 3
            # create regex to find permissions and apply it on second line of slk list output
            pattern = re.compile("[r-][w-][x-][r-][w-][x-][r-][w-][x-]")
            result = re.search(pattern, output_list.stdout.split('\n')[0])
            # did not find permission pattern in string => something wrong
            assert result is not None
            # extract permissions and check against provided permissions
            assert result.group(0) == permissions


    # @pytest.mark.dependency()
    # def test_chmod0(self):
    #     output = subprocess.run([ "slk", "chmod", "755", "/hsm/testing/prepared/list/one_file/file_01.txt" ], capture_output=True, encoding="ascii")
    #     assert output.returncode == 0

    # @pytest.mark.dependency(depends=["test_chmod0"], scope="class")
    # def test_perm1(self):
    #     output = subprocess.run(["slk", "list", "/hsm/testing/prepared/list/one_file"], capture_output=True, encoding="ascii")
    #     assert output.returncode == 0
    #     old_perm = re.split(r"\s+", output.stdout.split("\n")[1])[3]
    #     assert old_perm == "rw-r--r--"

# FIXME: Commented out, because chmod 000 destroys the file access...
#    @pytest.mark.dependency(depends=["test_perm1"], scope="class")
#    def test_chmod(self):
#        output = subprocess.run([ "slk", "chmod", "000", "/hsm/testing/prepared/list/one_file/file_01.txt" ], capture_output=True, encoding="ascii")
#        assert output.returncode == 0
#
#    @pytest.mark.dependency(depends=["test_chmod"], scope="class")
#    @pytest.mark.parametrize("slk_command,returncode", [
#       [
#         [ "slk", "list", "/hsm/testing/prepared/list/one_file"], 
#         0
#       ],
#    ])
#    def test_perm2(self, slk_command, returncode):
#        output = subprocess.run(slk_command, capture_output=True, encoding="ascii")
#        assert output.returncode == returncode
#        old_perm = re.split(r"\s+", output.stdout.split("\n")[1])[3]
#        # current behaviour File without read flag is not visisble..
#        assert old_perm == "---------"
#
