import pytest
import subprocess
from pathlib import Path
import utils

base= "/hsm/testing/archive-HM"
home= "/home/hadieh/gitslk"
test_archive= [  
        [   [ "slk", "archive", "-R" , home+"/archive/empty", base], "", "", 0, "archive test 01: Empty directory" ], 
        [   [ "slk", "archive", home+"/archive/afile$.txt", base], "", "", 0, "archive test 02: Filename with special character" ],
        [   [ "slk", "archive", "-R" , home+"/archive/rßäödeßwe", base], "", "", 0, "archive test 03: Directory with special character name" ],
        [   [ "slk", "archive", "-R" , home+"/archive/not_empty", base], "", "", 0, "archive test 04: Nested empty directories" ],
        [   [ "slk", "archive", "-R" , home+"/archive/folder-00", base], "", "", 0, "archive test 05: Nested full directories" ],
        [   [ "slk", "archive", "-R" , home+"/archive/folder-10", base], "", "", 0, "archive test 06: Nested directories with a large file" ],
        [   [ "slk", "archive", home+"/archive/large-file", base], "", "", 0, "archive test 07: A large file" ],
        [   [ "slk", "archive", home+"/archive/normal-file", base], "", "", 0, "archive test 08: Normal file" ],
        [   [ "slk", "archive", "-R" , home+"/archive/pictures", base], "", "", 0, "archive test 09: Pictures" ],
        [   [ "slk", "archive", "-R" , home+"/archive/read_only", base], "", "", 0, "archive test 10: No write permission" ],
        [   [ "slk", "archive", "-R" , home+"/archive/read_write", base], "", "", 0, "archive test 11: Both read and write permission" ],
        [   [ "slk", "archive", home+"/archive/readfile", base], "", "", 0, "archive test 12: File with only read permission" ],
        [   [ "slk", "archive", home+"/archive/no_write_permission", base], "", "", 0, "archive test 13: Simple archiving only" ],
        [   [ "slk", "chmod", "500" , base+"/no_write_permission"], "", "", 0, "archive test 14: changing permission of the archived folder" ],
        [   [ "slk", "archive", home+"/archive/normal-file", base+"no_write_permission"], "", "", 0, "archive test 15: archiving a file into no_write_permission directory" ],
        [   [ "slk", "list", "-R" , base+"/rßäödeßwe"], "", "", 0, "archive test 16: Listing Directory with special character" ],

            ]


class TestArchive:
    @pytest.mark.parametrize("slk_command,expected,stderr,returncode,comment", test_archive)

    def test_archive(self, slk_command, expected, stderr, returncode, comment):
        output = subprocess.run(slk_command, capture_output=True, encoding="ascii")
        assert output.returncode == returncode, comment+'; bad exit code: '+str(output.returncode)
        if (output.returncode == 0 and expected):
          assert output.stdout == expected, comment+'; bad stdout'
        if (stderr):
          assert output.stderr == stderr, comment+'; bad stderr'

