You will need

Via conda
* Python 3.9
* pytest
* pandas

Via `pip install`
* httplib2
* pytest-dependency
* warnings

start with `pytest test*.py`

or `pytest --tb=no test*.py` for short output

#### general
* [ ] Error messages should be printed into stderr
* [ ] Return Codes should be set according to the corresponding Unix tool (e.g. `slk list` like `ls`)
* [ ] Invalid calls like `slk blabla` should return `1`
#### slk list
* [ ] `slk list /path/to/file` should show the corresponding properties of the selected file
* [ ] `slk list /dir` should show all files of this folder, even if the user has no read permission for a specific file, as long as the user has execute and read permissions for `/dir`  
#### slk archive
* [ ] `slk archive file /hsm/folder/` should behave the same way as `slk archive bla /hsm/folder` 
#### slk retrieve
* [ ] `slk retrieve /hsm/folder/subfolder .` should download all files and store them in a new folder `subfolder`. It should NOT recreate the whole folder hierarchy beginning from `folder` 
* [ ] `slk retrieve /hsm/k204206/many_small_files/a/ .` should behave the same way as `slk retrieve /hsm/k204206/many_small_files/a .` 
#### slk chmod
* [ ] only the owner of a file should be able to change the permissions
#### slk owner
* [ ] only the owner of a file/namespace` should be able to change the owner
#### slk delete
* [ ] it shouldn't be possible to delete files of someone else, if you don't have write-permissions in the current folder
#### slk rename/move
* [ ] move and rename should be one command
* [ ] `slk rename /hsm/k204206/test/testing/foo/foo
       /hsm/k204206/renamed` should move the file and rename it to
       `renamed`
* [ ] It should be possible to rename a file into the name of an existing one and overwrite the existing one (currently: `ServerError`)
* [ ] only the owner should be able to overwrite a file by another file

#### no tests, but noteworthy issues
* [ ] `Size` of very small files in `slk list` has a whitespace between number and unit (`2 B`) while larger files have no whitespace (`2K`). Could produce issues for users who work with regular shell tools like awk.
* [ ] `Path` in `slk list` is empty for files in the selected namespace -> completely empty column which could cause issues for users of awk.


### Combined Test Cases

Preconditions:

* folders exist on hsm: `/hsm/testing/combined_tests/[ct01|ct02|...|ct20]`
* source files are available: `cp /work/bm0146/k204221/hsm_test_files/selected_hsm_testing_files_functional $TMP/functional_testing`


#### combined test 01

test: one file; list, archive, delete

```
slk list /hsm/testing/combined_tests/ct01 => 0 files
slk archive $TMP/functional_testing/CMIP6_pr_Amon_AWI-ESM-1-1-LR_historical_r1i1p1f1_gn_201001-201012.nc  /hsm/testing/combined_tests/ct01
slk list /hsm/testing/combined_tests/ct01 => 1 file
slk delete /hsm/testing/combined_tests/ct01/CMIP6_pr_Amon_AWI-ESM-1-1-LR_historical_r1i1p1f1_gn_201001-201012.nc
slk list /hsm/testing/combined_tests/ct01 => 0 files
```

#### combined test 02

test: one file; list, archive, rename, delete

```
slk list /hsm/testing/combined_tests/ct02 => 0 files
slk archive $TMP/functional_testing/CMIP6_tas_Amon_MPI-ESM1-2-LR_historical_r1i1p1f1_gn_191001-192912.nc  /hsm/testing/combined_tests/ct02
slk list /hsm/testing/combined_tests/ct02 => 1 file
slk rename CMIP6_tas_Amon_MPI-ESM1-2-LR_historical_r1i1p1f1_gn_191001-192912.nc cmip6_output.nc
slk list /hsm/testing/combined_tests/ct02 => 1 file, new name
slk delete /hsm/testing/combined_tests/ct02/cmip6_output.nc
slk list /hsm/testing/combined_tests/ct02 => 0 files
```

#### combined test 03

test: prepended `/` in the end of target directory works; chmod, owner, group

```
slk list /hsm/testing/combined_tests/ct03 => 0 files
slk archive $TMP/functional_testing/CMIP6_pr_Amon_AWI-ESM-1-1-LR_historical_r1i1p1f1_gn_201001-201012.nc  /hsm/testing/combined_tests/ct03
slk list /hsm/testing/combined_tests/ct03 => 1 file
slk chmod 777 /hsm/testing/combined_tests/ct03/CMIP6_pr_Amon_AWI-ESM-1-1-LR_historical_r1i1p1f1_gn_201001-201012.nc
slk list /hsm/testing/combined_tests/ct03 => 1 file, new permissions
slk owner TODO /hsm/testing/combined_tests/ct03/CMIP6_pr_Amon_AWI-ESM-1-1-LR_historical_r1i1p1f1_gn_201001-201012.nc
slk group TODO /hsm/testing/combined_tests/ct03/CMIP6_pr_Amon_AWI-ESM-1-1-LR_historical_r1i1p1f1_gn_201001-201012.nc
slk list /hsm/testing/combined_tests/ct03 => 1 file, new ownership and group
slk delete /hsm/testing/combined_tests/ct03/CMIP6_pr_Amon_AWI-ESM-1-1-LR_historical_r1i1p1f1_gn_201001-201012.nc
slk list /hsm/testing/combined_tests/ct03 => 0 files
```

#### combined test 04

test: multiple files archived by wildcard; multiple files deleted (?)

```
slk list /hsm/testing/combined_tests/ct04 => 0 files
slk archive $TMP/functional_testing/iow_day3d_t_no3_emep_201?.nc /hsm/testing/combined_tests/ct04
slk list /hsm/testing/combined_tests/ct04 => 5 file
slk delete /hsm/testing/combined_tests/ct04/TODO (do we need multiple cassl of delete?)
slk list /hsm/testing/combined_tests/ct04 => 0 files
```


#### combined test 05

test: archive a folder recursively => without `-r` => fail

```
slk list /hsm/testing/combined_tests/ct05 => 0 files
slk archive $TMP/functional_testing/ndep_recursive /hsm/testing/combined_tests/ct05 => should fail!!!
slk list /hsm/testing/combined_tests/ct05 => 0 files
if slk archive successful: slk delete /hsm/testing/combined_tests/ct05/ndep_recursive
```


#### combined test 06

test: archive a folder recursively => with `-r` => success; move

```
slk list /hsm/testing/combined_tests/ct06 => 0 files
slk archive -r $TMP/functional_testing/ndep_recursive /hsm/testing/combined_tests/ct06
slk list /hsm/testing/combined_tests/ct06 => 2 folders
slk list -r /hsm/testing/combined_tests/ct06 => 5 files; 2 folders
slk move /hsm/testing/combined_tests/ct06/cmaq/iow_day3d_t_no3_cmaq_2012.nc /hsm/testing/combined_tests/ct06/ndep_recursive/move_iow_day3d_t_no3_cmaq_2012_here/
slk list /hsm/testing/combined_tests/ct06/cmaq => 1 file
slk list /hsm/testing/combined_tests/ct06/move_iow_day3d_t_no3_cmaq_2012_here => 1 file
slk owner k204206 /hsm/testing/combined_tests/ct06/move_iow_day3d_t_no3_cmaq_2012_here/iow_day3d_t_no3_cmaq_2012.nc => success
slk list /hsm/testing/combined_tests/ct06/move_iow_day3d_t_no3_cmaq_2012_here
slk owner k204206 /hsm/testing/combined_tests/ct06/cmaq => only folder
slk list /hsm/testing/combined_tests/ct06
slk owner -r k204206 /hsm/testing/combined_tests/ct06/cmaq => success, also files
slk list /hsm/testing/combined_tests/ct06/cmaq
slk chmod 777 /hsm/testing/combined_tests/ct06/move_iow_day3d_t_no3_cmaq_2012_here/iow_day3d_t_no3_cmaq_2012.nc => success
slk list /hsm/testing/combined_tests/ct06/move_iow_day3d_t_no3_cmaq_2012_here
slk chmod 777 /hsm/testing/combined_tests/ct06/cmaq => only folder
slk list /hsm/testing/combined_tests/ct06
slk chmod -r 777 /hsm/testing/combined_tests/ct06/cmaq => success, also files
slk list /hsm/testing/combined_tests/ct06/cmaq
slk delete cmaq => fail
slk delete -r cmaq => success
slk delete -r move_iow_day3d_t_no3_cmaq_2012_here emep => success (but will fail now)
slk delete -r move_iow_day3d_t_no3_cmaq_2012_here
slk delete -r emep
```


#### combined test 07

test: slk tag and slk search

```
slk list /hsm/testing/combined_tests/ct07 => 0 files
slk archive $TMP/functional_testing/CMIP6_pr_Amon_AWI-ESM-1-1-LR_historical_r1i1p1f1_gn_201001-201012.nc  /hsm/testing/combined_tests/ct07
# the next command should fail; but as long as it does not and as long as `slk tag /hsm/testing/combined_tests/ct07/CMIP6_pr_Amon_AWI-ESM-1-1-LR_historical_r1i1p1f1_gn_201001-201012.nc` does not work, we do the following:
slk tag /hsm/testing/combined_tests/ct07 document.Author="RANDOM_STRING"
slk search '{"document.Author": "RANDOM_STRING"}' => get search id; should be one result
slk list SEARCH_ID => should list the archived file
slk delete SEARCH_ID
slk list /hsm/testing/combined_tests/ct07 => should be 0 files
```


#### combined test 08

test: timing issues

related issue: https://gitlab.cloud.dkrz.de/hsm/stronglink/collaboration/-/issues/56

```
slk archive $TMP/functional_testing/river_v04/warnow /hsm/testing/combined_tests/ct08
slk rename /hsm/testing/combined_tests/ct08/warnow warnow_v04
slk archive $TMP/functional_testing/river_v05/warnow /hsm/testing/combined_tests/ct08
slk rename /hsm/testing/combined_tests/ct08/warnow warnow_v05
slk list /hsm/k204221/testing/testing/combined_tests/ct08/warnow_v04 => should contain 3 files
slk list /hsm/k204221/testing/testing/combined_tests/ct08/warnow_v05 => should contain 4 files
```

bad output, that we get now:

```
k204221@mlogin102% slk list /hsm/k204221/testing/testing/combined_tests/ct08/warnow_v04
Filename                                  Path                                            Size   Mode        User           Group          Modified      Status      
warnow_river_phoswam_v04_15.nc                                                           84.3K   rwxr-xr-x   k204221        1076           30 Mar 2021               
warnow_river_phoswam_v04_35.nc                                                           84.3K   rwxr-xr-x   k204221        1076           30 Mar 2021               
warnow_river_phoswam_v04_ist.nc                                                          84.3K   rwxr-xr-x   k204221        1076           30 Mar 2021               
warnow_river_phoswam_v05_ist.nc                                                          84.3K   rw-r--r--   k204221        1076           30 Mar 2021               
Files 1-4 of 4

k204221@mlogin102% slk list /hsm/k204221/testing/testing/combined_tests/ct08/warnow_v05
WARNING: no results returned; user may not have permission to view files or search returns no results.
```


### tests that check whether specific issues were solved

Data needed to be prepared for testing:

* `top_folder/sub_folder/file_to_burn.txt`
* `dummy_yummy/y_y`

Folders located in `/scratch/k/k204221/slk_testing_tmp/test_data`(=TMP) after calling the data-generation-scripts

Create hsm folder: `/hsm/testing/resolved_issues` for testing


#### issue-solved 

test: move folder into own sub folder

related issue: https://gitlab.cloud.dkrz.de/hsm/stronglink/collaboration/-/issues/72

```
slk archive $TMP/issue_072/top_folder /hsm/testing/resolved_issues/issue_072
slk move /hsm/testing/resolved_issues/issue_072/top_folder /hsm/testing/resolved_issues/issue_072/top_folder/sub_folder => should throw an error
slk delete /hsm/testing/resolved_issues/issue_072/top_folder /hsm/testing/resolved_issues/issue_072/top_folder
```


#### issue-solved test 82

related issue: https://gitlab.cloud.dkrz.de/hsm/stronglink/collaboration/-/issues/82

```
slk archive $TMP/issue_082/dummy_yummy/y_y /hsm/testing/resolved_issues/issue_082 => should be successful
slk list /hsm/testing/resolved_issues/issue_082/y_y => one file
slk delete /hsm/testing/resolved_issues/issue_082/y_y
```

### TODOs

* more `slk retrieve` in the combined tests
* `slk archive` of several files; modify some local files; `slk archive` again; see if correct number of files was re-uploaded
* question: do we expect slk archive to create namespaces that do not exist yet?
