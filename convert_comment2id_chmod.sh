#!/bin/bash

cat $1 | \
sed -r 's/^      \[/      pytest.param(/g' | \
sed -r 's/^      \]/      )/g' | \
sed -r 's/,( )?comment,/,/g' | \
sed -r 's/, comment\+.*$//g' | \
sed -r 's/, ("chmod test [0-9]{2}: [^"]*")(.*)$/\2, id=\1/g' | \
cat
