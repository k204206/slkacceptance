import pytest
import subprocess
import time
import json
from pathlib import PurePath, Path
from subprocess import Popen, PIPE, STDOUT
import utils

class TestRetrieve:
    @pytest.mark.parametrize("slk_command,expected,returncode", [
      [
        [ 
          "slk", "retrieve", "/hsm/testing/prepared/list/one_file/file_01.txt", "."],
          "b026324c6904b2a9cb4b88d6d61c81d1",
          0
        ] 
      ]
    )
    def test_single_checksum(self, slk_command, expected, returncode):
        output = subprocess.run(slk_command, capture_output=True, encoding="ascii")
        path = PurePath(slk_command[-2])
        filename = str(PurePath(slk_command[-2]).name)
        md5sum = utils.md5(filename)
        if (expected):
          assert md5sum == expected
        assert output.returncode == returncode
        Path(filename).unlink()

    @pytest.mark.parametrize("slk_command,returncode",
      [
        [ 
         ["slk", "retrieve", "/hsm/testing/prepared/list/noreadpermission/foo", "."],
          1
        ] 
      ]
    )
    def test_retrieve_permission_denied(self, slk_command, returncode):
        purepath = PurePath(slk_command[-2])
        filename = str(PurePath(slk_command[-2]).name)
        path = Path(filename)
        # delete if it exists
        if (path.is_file()):
          path.unlink()
        output = subprocess.run(slk_command, capture_output=True, encoding="ascii")
        assert not path.is_file()
        assert output.returncode == returncode
        if (path.is_file()):
          path.unlink()

    @pytest.mark.parametrize("slk_command,returncode",
      [
        [ 
         ["slk", "retrieve", "/hsm/testing/prepared/list/two_files/", "two_files_test"],
          1
        ] 
      ]
    )
    def test_retrieve_folder(self, slk_command, returncode):
      output = subprocess.run(slk_command, capture_output=True, encoding="ascii")
      path = Path(slk_command[-1])
      # Folder has been created
      assert path.is_dir()
      # Folder has been created
      retrieved_path = Path(slk_command[-1] + "/two_files")
      assert retrieved_path.is_dir()
      if path.is_dir():
        path.rmdir()
