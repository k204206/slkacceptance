import pytest
import subprocess
import utils
from pathlib import Path

test_cases = [
      [
        [ "slk", "list", "/hsm/testing/prepared/permissions/folder_777_25301"],
        "",
        "slk list /hsm/testing/prepared/permissions/folder_777_25301",
        """Filename                                  Path                                            Size   Mode        User           Group          Modified      Status      
test_466.txt                                                                               5 B   r--rw-rw-   k204221        1076           26 Mar 2021               
test_606.txt                                                                               5 B   rw----rw-   k204221        1076           26 Mar 2021               
test_626.txt                                                                               5 B   rw--w-rw-   k204221        1076           26 Mar 2021               
test_646.txt                                                                               5 B   rw-r--rw-   k204221        1076           26 Mar 2021               
test_660.txt                                                                               5 B   rw-rw----   k204221        1076           26 Mar 2021               
test_662.txt                                                                               5 B   rw-rw--w-   k204221        1076           26 Mar 2021               
test_664.txt                                                                               5 B   rw-rw-r--   k204221        1076           26 Mar 2021               
test_666.txt                                                                               5 B   rw-rw-rw-   k204221        1076           26 Mar 2021               
Files: 1""",
        "",
        0,
        """Filename                                  Path                                                Size   Mode        User           Group          Modified      Status 
test_066.txt                              /hsm/testing/prepared/permissions/folder_777_25301  5 B   ---rw-rw-   k204221        1076           26 Mar 2021               
test_266.txt                              /hsm/testing/prepared/permissions/folder_777_25301  5 B   -w-rw-rw-   k204221        1076           26 Mar 2021               
test_466.txt                              /hsm/testing/prepared/permissions/folder_777_25301  5 B   r--rw-rw-   k204221        1076           26 Mar 2021               
test_606.txt                              /hsm/testing/prepared/permissions/folder_777_25301  5 B   rw----rw-   k204221        1076           26 Mar 2021               
test_626.txt                              /hsm/testing/prepared/permissions/folder_777_25301  5 B   rw--w-rw-   k204221        1076           26 Mar 2021               
test_646.txt                              /hsm/testing/prepared/permissions/folder_777_25301  5 B   rw-r--rw-   k204221        1076           26 Mar 2021               
test_660.txt                              /hsm/testing/prepared/permissions/folder_777_25301  5 B   rw-rw----   k204221        1076           26 Mar 2021               
test_662.txt                              /hsm/testing/prepared/permissions/folder_777_25301  5 B   rw-rw--w-   k204221        1076           26 Mar 2021               
test_664.txt                              /hsm/testing/prepared/permissions/folder_777_25301  5 B   rw-rw-r--   k204221        1076           26 Mar 2021               
test_666.txt                              /hsm/testing/prepared/permissions/folder_777_25301  5 B   rw-rw-rw-   k204221        1076           26 Mar 2021               
Files: 10""",
        "",
        0
      ],
      [[ "slk", "list", "/hsm/testing/prepared/permissions/folder_770_25301"],
        "",
        "slk list /hsm/testing/prepared/permissions/folder_770_25301",
        """Filename                                  Path                                            Size   Mode        User           Group          Modified      Status      
test_466.txt                                                                               5 B   r--rw-rw-   k204221        1076           26 Mar 2021               
test_606.txt                                                                               5 B   rw----rw-   k204221        1076           26 Mar 2021               
test_626.txt                                                                               5 B   rw--w-rw-   k204221        1076           26 Mar 2021               
test_646.txt                                                                               5 B   rw-r--rw-   k204221        1076           26 Mar 2021               
test_660.txt                                                                               5 B   rw-rw----   k204221        1076           26 Mar 2021               
test_662.txt                                                                               5 B   rw-rw--w-   k204221        1076           26 Mar 2021               
test_664.txt                                                                               5 B   rw-rw-r--   k204221        1076           26 Mar 2021               
test_666.txt                                                                               5 B   rw-rw-rw-   k204221        1076           26 Mar 2021               
Files: 1""",
        "",
        0,
        """Filename                                  Path                                                Size   Mode        User           Group          Modified      Status 
test_066.txt                              /hsm/testing/prepared/permissions/folder_770_25301  5 B   ---rw-rw-   k204221        1076           26 Mar 2021               
test_266.txt                              /hsm/testing/prepared/permissions/folder_770_25301  5 B   -w-rw-rw-   k204221        1076           26 Mar 2021               
test_466.txt                              /hsm/testing/prepared/permissions/folder_770_25301  5 B   r--rw-rw-   k204221        1076           26 Mar 2021               
test_606.txt                              /hsm/testing/prepared/permissions/folder_770_25301  5 B   rw----rw-   k204221        1076           26 Mar 2021               
test_626.txt                              /hsm/testing/prepared/permissions/folder_770_25301  5 B   rw--w-rw-   k204221        1076           26 Mar 2021               
test_646.txt                              /hsm/testing/prepared/permissions/folder_770_25301  5 B   rw-r--rw-   k204221        1076           26 Mar 2021               
test_660.txt                              /hsm/testing/prepared/permissions/folder_770_25301  5 B   rw-rw----   k204221        1076           26 Mar 2021               
test_662.txt                              /hsm/testing/prepared/permissions/folder_770_25301  5 B   rw-rw--w-   k204221        1076           26 Mar 2021               
test_664.txt                              /hsm/testing/prepared/permissions/folder_770_25301  5 B   rw-rw-r--   k204221        1076           26 Mar 2021               
test_666.txt                              /hsm/testing/prepared/permissions/folder_770_25301  5 B   rw-rw-rw-   k204221        1076           26 Mar 2021               
Files: 10""",
        "",
        0
      ],
      [[ "slk", "list", "/hsm/testing/prepared/permissions/folder_707_25301"],
        "",
        "slk list /hsm/testing/prepared/permissions/folder_707_25301",
        """Filename                                  Path                                            Size   Mode        User           Group          Modified      Status      
test_466.txt                                                                               5 B   r--rw-rw-   k204221        1076           26 Mar 2021               
test_606.txt                                                                               5 B   rw----rw-   k204221        1076           26 Mar 2021               
test_626.txt                                                                               5 B   rw--w-rw-   k204221        1076           26 Mar 2021               
test_646.txt                                                                               5 B   rw-r--rw-   k204221        1076           26 Mar 2021               
test_660.txt                                                                               5 B   rw-rw----   k204221        1076           26 Mar 2021               
test_662.txt                                                                               5 B   rw-rw--w-   k204221        1076           26 Mar 2021               
test_664.txt                                                                               5 B   rw-rw-r--   k204221        1076           26 Mar 2021               
test_666.txt                                                                               5 B   rw-rw-rw-   k204221        1076           26 Mar 2021               
Files: 1""",
        "",
        0,
        """Filename                                  Path                                                Size   Mode        User           Group          Modified      Status 
test_066.txt                              /hsm/testing/prepared/permissions/folder_707_25301  5 B   ---rw-rw-   k204221        1076           26 Mar 2021               
test_266.txt                              /hsm/testing/prepared/permissions/folder_707_25301  5 B   -w-rw-rw-   k204221        1076           26 Mar 2021               
test_466.txt                              /hsm/testing/prepared/permissions/folder_707_25301  5 B   r--rw-rw-   k204221        1076           26 Mar 2021               
test_606.txt                              /hsm/testing/prepared/permissions/folder_707_25301  5 B   rw----rw-   k204221        1076           26 Mar 2021               
test_626.txt                              /hsm/testing/prepared/permissions/folder_707_25301  5 B   rw--w-rw-   k204221        1076           26 Mar 2021               
test_646.txt                              /hsm/testing/prepared/permissions/folder_707_25301  5 B   rw-r--rw-   k204221        1076           26 Mar 2021               
test_660.txt                              /hsm/testing/prepared/permissions/folder_707_25301  5 B   rw-rw----   k204221        1076           26 Mar 2021               
test_662.txt                              /hsm/testing/prepared/permissions/folder_707_25301  5 B   rw-rw--w-   k204221        1076           26 Mar 2021               
test_664.txt                              /hsm/testing/prepared/permissions/folder_707_25301  5 B   rw-rw-r--   k204221        1076           26 Mar 2021               
test_666.txt                              /hsm/testing/prepared/permissions/folder_707_25301  5 B   rw-rw-rw-   k204221        1076           26 Mar 2021               
Files: 10""",
        "",
        0
      ],
      [[ "slk", "list", "/hsm/testing/prepared/permissions/folder_577_25301"],
        "",
        "slk list /hsm/testing/prepared/permissions/folder_577_25301",
        """Filename                                  Path                                            Size   Mode        User           Group          Modified      Status      
test_466.txt                                                                               5 B   r--rw-rw-   k204221        1076           26 Mar 2021               
test_606.txt                                                                               5 B   rw----rw-   k204221        1076           26 Mar 2021               
test_626.txt                                                                               5 B   rw--w-rw-   k204221        1076           26 Mar 2021               
test_646.txt                                                                               5 B   rw-r--rw-   k204221        1076           26 Mar 2021               
test_660.txt                                                                               5 B   rw-rw----   k204221        1076           26 Mar 2021               
test_662.txt                                                                               5 B   rw-rw--w-   k204221        1076           26 Mar 2021               
test_664.txt                                                                               5 B   rw-rw-r--   k204221        1076           26 Mar 2021               
test_666.txt                                                                               5 B   rw-rw-rw-   k204221        1076           26 Mar 2021               
Files: 1""",
        "",
        0,
        """Filename                                  Path                                                Size   Mode        User           Group          Modified      Status 
test_066.txt                              /hsm/testing/prepared/permissions/folder_577_25301  5 B   ---rw-rw-   k204221        1076           26 Mar 2021               
test_266.txt                              /hsm/testing/prepared/permissions/folder_577_25301  5 B   -w-rw-rw-   k204221        1076           26 Mar 2021               
test_466.txt                              /hsm/testing/prepared/permissions/folder_577_25301  5 B   r--rw-rw-   k204221        1076           26 Mar 2021               
test_606.txt                              /hsm/testing/prepared/permissions/folder_577_25301  5 B   rw----rw-   k204221        1076           26 Mar 2021               
test_626.txt                              /hsm/testing/prepared/permissions/folder_577_25301  5 B   rw--w-rw-   k204221        1076           26 Mar 2021               
test_646.txt                              /hsm/testing/prepared/permissions/folder_577_25301  5 B   rw-r--rw-   k204221        1076           26 Mar 2021               
test_660.txt                              /hsm/testing/prepared/permissions/folder_577_25301  5 B   rw-rw----   k204221        1076           26 Mar 2021               
test_662.txt                              /hsm/testing/prepared/permissions/folder_577_25301  5 B   rw-rw--w-   k204221        1076           26 Mar 2021               
test_664.txt                              /hsm/testing/prepared/permissions/folder_577_25301  5 B   rw-rw-r--   k204221        1076           26 Mar 2021               
test_666.txt                              /hsm/testing/prepared/permissions/folder_577_25301  5 B   rw-rw-rw-   k204221        1076           26 Mar 2021               
Files: 10""",
        "",
        0
      ],
      [["slk", "list", "/hsm/testing/prepared/permissions/folder_677_25301"],
        "",
        "slk list /hsm/testing/prepared/permissions/folder_677_25301",
       "",
       "cannot list content of directory /hsm/testing/prepared/permissions/folder_677_25301:execute permissions missing",
       1,
       "",
       "cannot list content of directory /hsm/testing/prepared/permissions/folder_677_25301:execute permissions missing",
       1
      ],
      [["slk", "list", "/hsm/testing/prepared/permissions/folder_377_25301"],
        "",
        "slk list /hsm/testing/prepared/permissions/folder_377_25301",
       "",
       "cannot open directory /hsm/testing/prepared/permissions/folder_377_25301: Permission denied",
       1,
       "",
       "cannot open directory /hsm/testing/prepared/permissions/folder_377_25301: Permission denied",
       1
      ],
      [["slk", "list", "/hsm/testing/prepared/permissions/folder_077_25301"],
        "",
        "slk list /hsm/testing/prepared/permissions/folder_077_25301",
       "",
       "cannot open directory /hsm/testing/prepared/permissions/folder_077_25301: Permission denied",
       1,
       "",
       "cannot open directory /hsm/testing/prepared/permissions/folder_077_25301: Permission denied",
       1
      ],
      [["slk", "list", "/hsm/testing/prepared/permissions/folder_777_22745"],
        "",
        "slk list /hsm/testing/prepared/permissions/folder_777_22745",
        """Filename                                  Path                                            Size   Mode        User           Group          Modified      Status      
test_466.txt                                                                               5 B   r--rw-rw-   k204206        1076           26 Mar 2021               
test_606.txt                                                                               5 B   rw----rw-   k204206        1076           26 Mar 2021               
test_626.txt                                                                               5 B   rw--w-rw-   k204206        1076           26 Mar 2021               
test_646.txt                                                                               5 B   rw-r--rw-   k204206        1076           26 Mar 2021               
test_660.txt                                                                               5 B   rw-rw----   k204206        1076           26 Mar 2021               
test_662.txt                                                                               5 B   rw-rw--w-   k204206        1076           26 Mar 2021               
test_664.txt                                                                               5 B   rw-rw-r--   k204206        1076           26 Mar 2021               
test_666.txt                                                                               5 B   rw-rw-rw-   k204206        1076           26 Mar 2021               
Files: 1""",
        "",
        0,
        """Filename                                  Path                                                Size   Mode        User           Group          Modified      Status 
test_066.txt                              /hsm/testing/prepared/permissions/folder_777_22745  5 B   ---rw-rw-   k204206        1076           26 Mar 2021               
test_266.txt                              /hsm/testing/prepared/permissions/folder_777_22745  5 B   -w-rw-rw-   k204206        1076           26 Mar 2021               
test_466.txt                              /hsm/testing/prepared/permissions/folder_777_22745  5 B   r--rw-rw-   k204206        1076           26 Mar 2021               
test_606.txt                              /hsm/testing/prepared/permissions/folder_777_22745  5 B   rw----rw-   k204206        1076           26 Mar 2021               
test_626.txt                              /hsm/testing/prepared/permissions/folder_777_22745  5 B   rw--w-rw-   k204206        1076           26 Mar 2021               
test_646.txt                              /hsm/testing/prepared/permissions/folder_777_22745  5 B   rw-r--rw-   k204206        1076           26 Mar 2021               
test_660.txt                              /hsm/testing/prepared/permissions/folder_777_22745  5 B   rw-rw----   k204206        1076           26 Mar 2021               
test_662.txt                              /hsm/testing/prepared/permissions/folder_777_22745  5 B   rw-rw--w-   k204206        1076           26 Mar 2021               
test_664.txt                              /hsm/testing/prepared/permissions/folder_777_22745  5 B   rw-rw-r--   k204206        1076           26 Mar 2021               
test_666.txt                              /hsm/testing/prepared/permissions/folder_777_22745  5 B   rw-rw-rw-   k204206        1076           26 Mar 2021               
Files: 10""",
        "",
        0
      ],
      [[ "slk", "list", "/hsm/testing/prepared/permissions/folder_770_22745"],
        "",
        "slk list /hsm/testing/prepared/permissions/folder_770_22745",
        """Filename                                  Path                                            Size   Mode        User           Group          Modified      Status      
test_466.txt                                                                               5 B   r--rw-rw-   k204206        1076           26 Mar 2021               
test_606.txt                                                                               5 B   rw----rw-   k204206        1076           26 Mar 2021               
test_626.txt                                                                               5 B   rw--w-rw-   k204206        1076           26 Mar 2021               
test_646.txt                                                                               5 B   rw-r--rw-   k204206        1076           26 Mar 2021               
test_660.txt                                                                               5 B   rw-rw----   k204206        1076           26 Mar 2021               
test_662.txt                                                                               5 B   rw-rw--w-   k204206        1076           26 Mar 2021               
test_664.txt                                                                               5 B   rw-rw-r--   k204206        1076           26 Mar 2021               
test_666.txt                                                                               5 B   rw-rw-rw-   k204206        1076           26 Mar 2021               
Files: 1""",
        "",
        0,
        """Filename                                  Path                                                Size   Mode        User           Group          Modified      Status 
test_066.txt                              /hsm/testing/prepared/permissions/folder_770_22745  5 B   ---rw-rw-   k204206        1076           26 Mar 2021               
test_266.txt                              /hsm/testing/prepared/permissions/folder_770_22745  5 B   -w-rw-rw-   k204206        1076           26 Mar 2021               
test_466.txt                              /hsm/testing/prepared/permissions/folder_770_22745  5 B   r--rw-rw-   k204206        1076           26 Mar 2021               
test_606.txt                              /hsm/testing/prepared/permissions/folder_770_22745  5 B   rw----rw-   k204206        1076           26 Mar 2021               
test_626.txt                              /hsm/testing/prepared/permissions/folder_770_22745  5 B   rw--w-rw-   k204206        1076           26 Mar 2021               
test_646.txt                              /hsm/testing/prepared/permissions/folder_770_22745  5 B   rw-r--rw-   k204206        1076           26 Mar 2021               
test_660.txt                              /hsm/testing/prepared/permissions/folder_770_22745  5 B   rw-rw----   k204206        1076           26 Mar 2021               
test_662.txt                              /hsm/testing/prepared/permissions/folder_770_22745  5 B   rw-rw--w-   k204206        1076           26 Mar 2021               
test_664.txt                              /hsm/testing/prepared/permissions/folder_770_22745  5 B   rw-rw-r--   k204206        1076           26 Mar 2021               
test_666.txt                              /hsm/testing/prepared/permissions/folder_770_22745  5 B   rw-rw-rw-   k204206        1076           26 Mar 2021               
Files: 10""",
        "",
        0
      ],
      [[ "slk", "list", "/hsm/testing/prepared/permissions/folder_077_22745"],
        "",
        "slk list /hsm/testing/prepared/permissions/folder_077_22745",
        """Filename                                  Path                                            Size   Mode        User           Group          Modified      Status      
test_466.txt                                                                               5 B   r--rw-rw-   k204206        1076           26 Mar 2021               
test_606.txt                                                                               5 B   rw----rw-   k204206        1076           26 Mar 2021               
test_626.txt                                                                               5 B   rw--w-rw-   k204206        1076           26 Mar 2021               
test_646.txt                                                                               5 B   rw-r--rw-   k204206        1076           26 Mar 2021               
test_660.txt                                                                               5 B   rw-rw----   k204206        1076           26 Mar 2021               
test_662.txt                                                                               5 B   rw-rw--w-   k204206        1076           26 Mar 2021               
test_664.txt                                                                               5 B   rw-rw-r--   k204206        1076           26 Mar 2021               
test_666.txt                                                                               5 B   rw-rw-rw-   k204206        1076           26 Mar 2021               
Files: 1""",
        "",
        0,
        """Filename                                  Path                                                Size   Mode        User           Group          Modified      Status 
test_066.txt                              /hsm/testing/prepared/permissions/folder_077_22745  5 B   ---rw-rw-   k204206        1076           26 Mar 2021               
test_266.txt                              /hsm/testing/prepared/permissions/folder_077_22745  5 B   -w-rw-rw-   k204206        1076           26 Mar 2021               
test_466.txt                              /hsm/testing/prepared/permissions/folder_077_22745  5 B   r--rw-rw-   k204206        1076           26 Mar 2021               
test_606.txt                              /hsm/testing/prepared/permissions/folder_077_22745  5 B   rw----rw-   k204206        1076           26 Mar 2021               
test_626.txt                              /hsm/testing/prepared/permissions/folder_077_22745  5 B   rw--w-rw-   k204206        1076           26 Mar 2021               
test_646.txt                              /hsm/testing/prepared/permissions/folder_077_22745  5 B   rw-r--rw-   k204206        1076           26 Mar 2021               
test_660.txt                              /hsm/testing/prepared/permissions/folder_077_22745  5 B   rw-rw----   k204206        1076           26 Mar 2021               
test_662.txt                              /hsm/testing/prepared/permissions/folder_077_22745  5 B   rw-rw--w-   k204206        1076           26 Mar 2021               
test_664.txt                              /hsm/testing/prepared/permissions/folder_077_22745  5 B   rw-rw-r--   k204206        1076           26 Mar 2021               
test_666.txt                              /hsm/testing/prepared/permissions/folder_077_22745  5 B   rw-rw-rw-   k204206        1076           26 Mar 2021               
Files: 10""",
        "",
        0
      ],
      [[ "slk", "list", "/hsm/testing/prepared/permissions/folder_757_22745"],
        "",
        "slk list /hsm/testing/prepared/permissions/folder_757_22745",
        """Filename                                  Path                                            Size   Mode        User           Group          Modified      Status      
test_466.txt                                                                               5 B   r--rw-rw-   k204206        1076           26 Mar 2021               
test_606.txt                                                                               5 B   rw----rw-   k204206        1076           26 Mar 2021               
test_626.txt                                                                               5 B   rw--w-rw-   k204206        1076           26 Mar 2021               
test_646.txt                                                                               5 B   rw-r--rw-   k204206        1076           26 Mar 2021               
test_660.txt                                                                               5 B   rw-rw----   k204206        1076           26 Mar 2021               
test_662.txt                                                                               5 B   rw-rw--w-   k204206        1076           26 Mar 2021               
test_664.txt                                                                               5 B   rw-rw-r--   k204206        1076           26 Mar 2021               
test_666.txt                                                                               5 B   rw-rw-rw-   k204206        1076           26 Mar 2021               
Files: 1""",
        "",
        0,
        """Filename                                  Path                                                Size   Mode        User           Group          Modified      Status 
test_066.txt                              /hsm/testing/prepared/permissions/folder_757_22745  5 B   ---rw-rw-   k204206        1076           26 Mar 2021               
test_266.txt                              /hsm/testing/prepared/permissions/folder_757_22745  5 B   -w-rw-rw-   k204206        1076           26 Mar 2021               
test_466.txt                              /hsm/testing/prepared/permissions/folder_757_22745  5 B   r--rw-rw-   k204206        1076           26 Mar 2021               
test_606.txt                              /hsm/testing/prepared/permissions/folder_757_22745  5 B   rw----rw-   k204206        1076           26 Mar 2021               
test_626.txt                              /hsm/testing/prepared/permissions/folder_757_22745  5 B   rw--w-rw-   k204206        1076           26 Mar 2021               
test_646.txt                              /hsm/testing/prepared/permissions/folder_757_22745  5 B   rw-r--rw-   k204206        1076           26 Mar 2021               
test_660.txt                              /hsm/testing/prepared/permissions/folder_757_22745  5 B   rw-rw----   k204206        1076           26 Mar 2021               
test_662.txt                              /hsm/testing/prepared/permissions/folder_757_22745  5 B   rw-rw--w-   k204206        1076           26 Mar 2021               
test_664.txt                              /hsm/testing/prepared/permissions/folder_757_22745  5 B   rw-rw-r--   k204206        1076           26 Mar 2021               
test_666.txt                              /hsm/testing/prepared/permissions/folder_757_22745  5 B   rw-rw-rw-   k204206        1076           26 Mar 2021               
Files: 10""",
        "",
        0
      ],
      [[ "slk", "list", "/hsm/testing/prepared/permissions/folder_767_22745"],
        "",
        "slk list /hsm/testing/prepared/permissions/folder_767_22745",
       "",
       "cannot list content of directory /hsm/testing/prepared/permissions/folder_767_22745:execute permissions missing",
       1,
       "",
       "cannot list content of directory /hsm/testing/prepared/permissions/folder_767_22745:execute permissions missing",
       1
      ],
      [[ "slk", "list", "/hsm/testing/prepared/permissions/folder_737_22745"],
        "",
        "slk list /hsm/testing/prepared/permissions/folder_737_22745",
       "",
       "cannot open directory /hsm/testing/prepared/permissions/folder_737_22745: Permission denied",
       1,
       "",
       "cannot open directory /hsm/testing/prepared/permissions/folder_737_22745: Permission denied",
       1
      ],
      [[ "slk", "list", "/hsm/testing/prepared/permissions/folder_707_22745"],
        "",
        "slk list /hsm/testing/prepared/permissions/folder_707_22745",
       "",
       "cannot open directory /hsm/testing/prepared/permissions/folder_707_22745: Permission denied",
       1,
       "",
       "cannot open directory /hsm/testing/prepared/permissions/folder_707_22745: Permission denied",
       1
      ],
      [[ "slk", "list", "/hsm/testing/prepared/permissions/folder_777_900"],
        "",
        "slk list /hsm/testing/prepared/permissions/folder_777_900",
        """Filename                                  Path                                            Size   Mode        User           Group          Modified      Status      
test_466.txt                                                                               5 B   r--rw-rw-   stronglink     900            26 Mar 2021               
test_606.txt                                                                               5 B   rw----rw-   stronglink     900            26 Mar 2021               
test_626.txt                                                                               5 B   rw--w-rw-   stronglink     900            26 Mar 2021               
test_646.txt                                                                               5 B   rw-r--rw-   stronglink     900            26 Mar 2021               
test_660.txt                                                                               5 B   rw-rw----   stronglink     900            26 Mar 2021               
test_662.txt                                                                               5 B   rw-rw--w-   stronglink     900            26 Mar 2021               
test_664.txt                                                                               5 B   rw-rw-r--   stronglink     900            26 Mar 2021               
test_666.txt                                                                               5 B   rw-rw-rw-   stronglink     900            26 Mar 2021               
Files: 1""",
        "",
        0,
        """Filename                                  Path                                                Size   Mode        User           Group          Modified      Status 
test_066.txt                              /hsm/testing/prepared/permissions/folder_777_900    5 B   ---rw-rw-   stronglink     900            26 Mar 2021               
test_266.txt                              /hsm/testing/prepared/permissions/folder_777_900    5 B   -w-rw-rw-   stronglink     900            26 Mar 2021               
test_466.txt                              /hsm/testing/prepared/permissions/folder_777_900    5 B   r--rw-rw-   stronglink     900            26 Mar 2021               
test_606.txt                              /hsm/testing/prepared/permissions/folder_777_900    5 B   rw----rw-   stronglink     900            26 Mar 2021               
test_626.txt                              /hsm/testing/prepared/permissions/folder_777_900    5 B   rw--w-rw-   stronglink     900            26 Mar 2021               
test_646.txt                              /hsm/testing/prepared/permissions/folder_777_900    5 B   rw-r--rw-   stronglink     900            26 Mar 2021               
test_660.txt                              /hsm/testing/prepared/permissions/folder_777_900    5 B   rw-rw----   stronglink     900            26 Mar 2021               
test_662.txt                              /hsm/testing/prepared/permissions/folder_777_900    5 B   rw-rw--w-   stronglink     900            26 Mar 2021               
test_664.txt                              /hsm/testing/prepared/permissions/folder_777_900    5 B   rw-rw-r--   stronglink     900            26 Mar 2021               
test_666.txt                              /hsm/testing/prepared/permissions/folder_777_900    5 B   rw-rw-rw-   stronglink     900            26 Mar 2021               
Files: 10""",
        "",
        0
      ],
      [[ "slk", "list", "/hsm/testing/prepared/permissions/folder_077_900"],
        "",
        "slk list /hsm/testing/prepared/permissions/folder_077_900",
        """Filename                                  Path                                            Size   Mode        User           Group          Modified      Status      
test_466.txt                                                                               5 B   r--rw-rw-   stronglink     900            26 Mar 2021               
test_606.txt                                                                               5 B   rw----rw-   stronglink     900            26 Mar 2021               
test_626.txt                                                                               5 B   rw--w-rw-   stronglink     900            26 Mar 2021               
test_646.txt                                                                               5 B   rw-r--rw-   stronglink     900            26 Mar 2021               
test_660.txt                                                                               5 B   rw-rw----   stronglink     900            26 Mar 2021               
test_662.txt                                                                               5 B   rw-rw--w-   stronglink     900            26 Mar 2021               
test_664.txt                                                                               5 B   rw-rw-r--   stronglink     900            26 Mar 2021               
test_666.txt                                                                               5 B   rw-rw-rw-   stronglink     900            26 Mar 2021               
Files: 1""",
        "",
        0,
        """Filename                                  Path                                                Size   Mode        User           Group          Modified      Status 
test_066.txt                              /hsm/testing/prepared/permissions/folder_077_900     5 B   ---rw-rw-   stronglink     900            26 Mar 2021               
test_266.txt                              /hsm/testing/prepared/permissions/folder_077_900     5 B   -w-rw-rw-   stronglink     900            26 Mar 2021               
test_466.txt                              /hsm/testing/prepared/permissions/folder_077_900     5 B   r--rw-rw-   stronglink     900            26 Mar 2021               
test_606.txt                              /hsm/testing/prepared/permissions/folder_077_900     5 B   rw----rw-   stronglink     900            26 Mar 2021               
test_626.txt                              /hsm/testing/prepared/permissions/folder_077_900     5 B   rw--w-rw-   stronglink     900            26 Mar 2021               
test_646.txt                              /hsm/testing/prepared/permissions/folder_077_900     5 B   rw-r--rw-   stronglink     900            26 Mar 2021               
test_660.txt                              /hsm/testing/prepared/permissions/folder_077_900     5 B   rw-rw----   stronglink     900            26 Mar 2021               
test_662.txt                              /hsm/testing/prepared/permissions/folder_077_900     5 B   rw-rw--w-   stronglink     900            26 Mar 2021               
test_664.txt                              /hsm/testing/prepared/permissions/folder_077_900     5 B   rw-rw-r--   stronglink     900            26 Mar 2021               
test_666.txt                              /hsm/testing/prepared/permissions/folder_077_900     5 B   rw-rw-rw-   stronglink     900            26 Mar 2021               
Files: 10""",
        "",
        0
      ],
      [[ "slk", "list", "/hsm/testing/prepared/permissions/folder_707_900"],
        "",
        "slk list /hsm/testing/prepared/permissions/folder_707_900",
        """Filename                                  Path                                            Size   Mode        User           Group          Modified      Status      
test_466.txt                                                                               5 B   r--rw-rw-   stronglink     900            26 Mar 2021               
test_606.txt                                                                               5 B   rw----rw-   stronglink     900            26 Mar 2021               
test_626.txt                                                                               5 B   rw--w-rw-   stronglink     900            26 Mar 2021               
test_646.txt                                                                               5 B   rw-r--rw-   stronglink     900            26 Mar 2021               
test_660.txt                                                                               5 B   rw-rw----   stronglink     900            26 Mar 2021               
test_662.txt                                                                               5 B   rw-rw--w-   stronglink     900            26 Mar 2021               
test_664.txt                                                                               5 B   rw-rw-r--   stronglink     900            26 Mar 2021               
test_666.txt                                                                               5 B   rw-rw-rw-   stronglink     900            26 Mar 2021               
Files: 1""",
        "",
        0,
        """Filename                                  Path                                                Size   Mode        User           Group          Modified      Status 
test_066.txt                              /hsm/testing/prepared/permissions/folder_707_900    5 B   ---rw-rw-   stronglink     900            26 Mar 2021               
test_266.txt                              /hsm/testing/prepared/permissions/folder_707_900    5 B   -w-rw-rw-   stronglink     900            26 Mar 2021               
test_466.txt                              /hsm/testing/prepared/permissions/folder_707_900    5 B   r--rw-rw-   stronglink     900            26 Mar 2021               
test_606.txt                              /hsm/testing/prepared/permissions/folder_707_900    5 B   rw----rw-   stronglink     900            26 Mar 2021               
test_626.txt                              /hsm/testing/prepared/permissions/folder_707_900    5 B   rw--w-rw-   stronglink     900            26 Mar 2021               
test_646.txt                              /hsm/testing/prepared/permissions/folder_707_900    5 B   rw-r--rw-   stronglink     900            26 Mar 2021               
test_660.txt                              /hsm/testing/prepared/permissions/folder_707_900    5 B   rw-rw----   stronglink     900            26 Mar 2021               
test_662.txt                              /hsm/testing/prepared/permissions/folder_707_900    5 B   rw-rw--w-   stronglink     900            26 Mar 2021               
test_664.txt                              /hsm/testing/prepared/permissions/folder_707_900    5 B   rw-rw-r--   stronglink     900            26 Mar 2021               
test_666.txt                              /hsm/testing/prepared/permissions/folder_707_900    5 B   rw-rw-rw-   stronglink     900            26 Mar 2021               
Files: 10""",
        "",
        0
      ],
      [[ "slk", "list", "/hsm/testing/prepared/permissions/folder_775_900"],
        "",
        "slk list /hsm/testing/prepared/permissions/folder_775_900",
        """Filename                                  Path                                            Size   Mode        User           Group          Modified      Status      
test_466.txt                                                                               5 B   r--rw-rw-   stronglink     900            26 Mar 2021               
test_606.txt                                                                               5 B   rw----rw-   stronglink     900            26 Mar 2021               
test_626.txt                                                                               5 B   rw--w-rw-   stronglink     900            26 Mar 2021               
test_646.txt                                                                               5 B   rw-r--rw-   stronglink     900            26 Mar 2021               
test_664.txt                                                                               5 B   rw-rw-r--   stronglink     900            26 Mar 2021               
test_666.txt                                                                               5 B   rw-rw-rw-   stronglink     900            26 Mar 2021               
Files: 1""",
        "",
        0,
        """Filename                                  Path                                              Size   Mode        User           Group          Modified      Status 
test_066.txt                              /hsm/testing/prepared/permissions/folder_775_900  5 B   ---rw-rw-   stronglink     900            26 Mar 2021               
test_266.txt                              /hsm/testing/prepared/permissions/folder_775_900  5 B   -w-rw-rw-   stronglink     900            26 Mar 2021               
test_466.txt                              /hsm/testing/prepared/permissions/folder_775_900  5 B   r--rw-rw-   stronglink     900            26 Mar 2021               
test_606.txt                              /hsm/testing/prepared/permissions/folder_775_900  5 B   rw----rw-   stronglink     900            26 Mar 2021               
test_626.txt                              /hsm/testing/prepared/permissions/folder_775_900  5 B   rw--w-rw-   stronglink     900            26 Mar 2021               
test_646.txt                              /hsm/testing/prepared/permissions/folder_775_900  5 B   rw-r--rw-   stronglink     900            26 Mar 2021               
test_660.txt                              /hsm/testing/prepared/permissions/folder_775_900  5 B   rw-rw-rw-   stronglink     900            26 Mar 2021               
test_662.txt                              /hsm/testing/prepared/permissions/folder_775_900  5 B   rw-rw--w-   stronglink     900            26 Mar 2021               
test_664.txt                              /hsm/testing/prepared/permissions/folder_775_900  5 B   rw-rw-r--   stronglink     900            26 Mar 2021               
test_666.txt                              /hsm/testing/prepared/permissions/folder_775_900  5 B   rw-rw-rw-   stronglink     900            26 Mar 2021               
Files: 10""",
        "",
        0
      ],
      [[ "slk", "list", "/hsm/testing/prepared/permissions/folder_776_900"],
        "",
        "slk list /hsm/testing/prepared/permissions/folder_776_900",
       "",
       "cannot list content of directory /hsm/testing/prepared/permissions/folder_776_900:execute permissions missing",
       1,
       "",
       "cannot list content of directory /hsm/testing/prepared/permissions/folder_776_900:execute permissions missing",
       1
      ],
      [[ "slk", "list", "/hsm/testing/prepared/permissions/folder_773_900"],
        "",
        "slk list /hsm/testing/prepared/permissions/folder_773_900",
       "",
       "cannot open directory /hsm/testing/prepared/permissions/folder_773_900: Permission denied",
       1,
       "",
       "cannot open directory /hsm/testing/prepared/permissions/folder_773_900: Permission denied",
       1
      ],
      [[ "slk", "list", "/hsm/testing/prepared/permissions/folder_770_900"],
        "",
        "slk list /hsm/testing/prepared/permissions/folder_770_900",
       "",
       "cannot open directory /hsm/testing/prepared/permissions/folder_770_900: Permission denied",
       1,
       "",
       "cannot open directory /hsm/testing/prepared/permissions/folder_770_900: Permission denied",
       1
      ]
    ]

class TestList:
    @pytest.mark.parametrize("slk_command,comment,str_cmd,expected,stderr,returncode,stdout_nice,stderr_nice,returncode_nice", test_cases)

    def test_list_permissions(self, slk_command, comment, str_cmd, expected, stderr, returncode, stdout_nice, stderr_nice, returncode_nice):
        output = subprocess.run(slk_command, capture_output=True, encoding="ascii")
        assert output.returncode == returncode
        if (output.returncode == 0 and expected):
          if "WARNING" in output.stdout: 
            assert output.stdout == expected
          else:
            utils.check_list_output(output.stdout, expected)
         
        if (stderr):
          assert output.stderr == stderr


