import pytest
import hashlib
import pandas
import httplib2
from pathlib import Path
import netrc
import json
import tempfile

def md5(fname):
    hash_md5 = hashlib.md5()
    with open(fname, "rb") as f:
        for chunk in iter(lambda: f.read(4096), b""):
            hash_md5.update(chunk)
    return hash_md5.hexdigest()

def login_netrc(machine_name):
  info = netrc.netrc()
  login, account, password = info.authenticators(machine_name)
  httplib2.debuglevel     = 0
  http                    = httplib2.Http(disable_ssl_certificate_validation=True)
  content_type_header     = "application/json"
  url = "https://stronglink.hsm.dkrz.de/api/v2/authentication"
  headers = {'Content-Type': content_type_header}
  pload = {
              "data": {
              "type": "authentication",
              "attributes": {
                  "name": login,
                  "password": password, 
                  "domain": "ldap"
                }
              }
          }
  response, content = http.request( url, 'POST', json.dumps(pload), headers=headers)
  parsed_content = json.loads(content)
  session_key = parsed_content["data"]["attributes"]["session_key"]
  with open( str(Path.home()) + "/.slk/config.json", 'w') as writer:
    obj = {
      "user": login,
      "session_key":session_key
    }
    json.dump(obj, writer)

def parse_ls(inp):
  lines = inp.splitlines()

def check_list_output(strA, strB):
  __tracebackhide__ = True
  f = tempfile.NamedTemporaryFile(mode='w+t')
  f2 = tempfile.NamedTemporaryFile(mode='w+t')
  try:
      f.writelines([strA])
      f2.writelines([strB])
      f.seek(0)
      f2.seek(0)
      dataframe1 = pandas.read_fwf(f.name)
      del dataframe1["Modified"]
      dataframe2 = pandas.read_fwf(f2.name)
      del dataframe2["Modified"]
      if not dataframe1.equals(dataframe2):
        pytest.fail("slk list output does not correspond to the expected value")
  finally:
      f.close()

