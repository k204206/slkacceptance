import pytest
import subprocess
import utils
from pathlib import Path

# base_dir = '/hsm/testing/prepared'
base_dir = r'/hsm/k204221/testing/testing/prepared01'

test_cases = [
      [
        [ "./slk.sh", "delete", base_dir+"/delete/one_file/file_01.txt"],
        "", "", 0, "del test 01: delete individual file"
      ],
      [
        [ "./slk.sh", "delete", base_dir+"/delete/one_file"],
        "", "", 0, "del test 02: delete empty folder (no trailing slash)"
      ],
      [
        [ "./slk.sh", "delete", base_dir+r"/delete/one_file_special_char/file_$.txt"],
        "", "", 0, "del test 03: delete individual file with special character in name"
      ],
      [
        [ "./slk.sh", "delete", base_dir+"/delete/one_file_special_char/"],
        "", "", 0, "del test 04: delete empty folder (trailing slash)"
      ],
      [
        [ "./slk.sh", "delete", base_dir+"/delete/non_empty_folder_a"],
        "", None, 1, "del test 05: delete non-empty folder without -R"
        # todo: error message
      ],
      [
        [ "./slk.sh", "delete", "-R", base_dir+"/delete/non_empty_folder_b"],
        "", "", 0, "del test 06: delete non-empty folder with -R"
      ],
      [
        [ "./slk.sh", "delete", base_dir+"/delete/with_subfolder_and_file_a"],
        "", None, 1, "del test 07: delete non-empty folder with file and subfolder in it; without -R"
        # todo: error message
      ],
      [
        [ "./slk.sh", "delete", "-R", base_dir+"/delete/with_subfolder_and_file_b"],
        "", "", 0, "del test 08: delete non-empty folder with file and subfolder in it; with -R"
      ],
      [
        [ "./slk.sh", "delete", base_dir+"/delete/one_folder_with_file_in_subfolder_a"],
        "", None, 1, "del test 09: delete folder with non-empty subfolder in it; without -R"
        # todo: error message
      ],
      [
        [ "./slk.sh", "delete", "-R", base_dir+"/delete/one_folder_with_file_in_subfolder_b"],
        "", "", 0, "del test 10: delete folder with non-empty subfolder in it; with -R"
      ],
      [
        [ "./slk.sh", "delete", base_dir+r"/delete/one_folder_with_file_in_subfolder_c/subfolder/file_<p>.txt"],
        "", "", 0, "del test 11: delete individual file with another special character in name"
      ],
      [
        [ "./slk.sh", "delete", base_dir+"/delete/one_folder_with_file_in_subfolder_c"],
        "", None, 1, "del test 12: delete folder with empty subfolder in it; without -R"
        # todo: error message
      ],
      [
        [ "./slk.sh", "delete", base_dir+"/delete/non/existing/file.txt"],
        "", None, 1, "del test 13: delete non-existing file"
        # todo: error message
      ],
      [
        [ "./slk.sh", "delete", base_dir+"/delete/non/existing/folder/"],
        "", None, 1, "del test 14: delete non-existing folder; without -R"
        # todo: error message
      ],
      [
        [ "./slk.sh", "delete", "-R", base_dir+"/delete/non/existing/folder/"],
        "", None, 1, "del test 15: delete non-existing folder; with -R"
        # todo: error message
      ],
      [
        [ "./slk.sh", "delete", base_dir+"/delete/check_permissions_a/file_no_write.txt"],
        "", None, 1, "del test 16: delete file without write permissions"
        # todo: error message
      ],
      [
        [ "./slk.sh", "delete", base_dir+"/delete/check_permissions_a/file_no_read.txt"],
        "", "", 0, "del test 17: delete file without read permissions"
      ],
      [
        [ "./slk.sh", "delete", "-R", base_dir+"/delete/check_permissions_b"],
        "", None, 1, "del test 18: delete folder containg files without read and write permissions; with -R"
        # todo: error message
      ],
      [
        [ "./slk.sh", "delete", base_dir+"/delete/folder_no_read/file_with_read.txt"],
        "", "", 0, "del test 19: delete file in folder without read permissions"
      ]
    ]

class TestList:
    @pytest.mark.parametrize("slk_command,expected,stderr,returncode,comment", test_cases)

    def test_delete(self, slk_command, expected, stderr, returncode, comment):
        output = subprocess.run(slk_command, capture_output=True, encoding="ascii")
        assert output.returncode == returncode, comment+'; bad exit code: '+str(output.returncode)
        if (output.returncode == 0 and expected):
          assert output.stdout == expected, comment+'; bad stdout'
          
        if (stderr):
          assert output.stderr == stderr, comment+'; bad stderr'


