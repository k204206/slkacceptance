import pytest
import warnings
import subprocess
import re
from pathlib import Path

base_folder = "/hsm/k204206/rename_tests/"
test_folder_permissions_ignored = base_folder + "permissions/folder_rwxrwxrwx/"
test_folder_no_write_permission = base_folder + "permissions/folder_r_xr_xr_x/"

test_cases_should_fail = [
        pytest.param(
            ["slk", "rename", "/does/not/exist", "foo"], "ERROR: The path for the given file or folder does not exist.", 1, id="rename non existing path"
        ),
        pytest.param(
            ["slk", "rename", test_folder_permissions_ignored + "file0", "does_already_exist"], "ERROR: Unable to rename resource.", 1, id="rename file to a name which already exists"
        ),
        pytest.param(
            ["slk", "rename", test_folder_permissions_ignored + "file1", "."], "ERROR: Unable to rename resource.", 1, id="rename file to reserved POSIX name"
        ),
        # MOVING TARGET: Right know this is not possible, might change some day
        pytest.param(
            ["slk", "rename", test_folder_permissions_ignored + "file2", test_folder_permissions_ignored + "renamed_file2"], "ERROR: Unable to rename resource.", 1, id="move via rename"
        ),
        pytest.param(
            ["slk", "rename", "<", "file"], "ERROR: The path for the given file or folder does not exist.", 1, id="rename invalid name"
        )
      ]

test_cases_should_succeed = [
        pytest.param(
            ["slk", "rename", test_folder_permissions_ignored + "folder1", "folder1"], 0, id="rename folder to its own name (NO-OP)"
        ),
        pytest.param(
            ["slk", "rename", test_folder_permissions_ignored + "file3", "new_file_3"], 0, id="rename file"
        ),
        pytest.param(
            ["slk", "rename", test_folder_permissions_ignored + "folder2", "new_folder2"], 0, id="rename folder"
        ),
        pytest.param(
            ["slk", "rename", test_folder_permissions_ignored + "file4", "extremeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeely_long_name"], 0, id="rename file to something long"
        ),
        pytest.param(
            ["slk", "rename", test_folder_permissions_ignored + "no_read_write", "new_filenoreadwrite"], 0, id="rename a file without read and write permissions [this is possible in POSIX]"
        ),
    ]

class TestRename:

    @pytest.mark.parametrize("slk_command, expected_output, return_code", test_cases_should_fail)
    def test_should_fail(self, slk_command, expected_output, return_code):
        output = subprocess.run(slk_command, capture_output=True, encoding="ascii")
        assert output.returncode == return_code, "Wrong return code"
        if (not output.stderr and output.stdout):
          warnings.warn(UserWarning( "Output on stderr expected, but got on stdout"))
          assert expected_output == output.stdout.strip(), "Wrong output: " + output.stdout
        elif (not output.stderr):
          assert output.stderr, "Output on stderr expected"
        else:
          assert expected_output == output.stderr.strip(), "Wrong output: " + output.stderr

    @pytest.mark.parametrize("slk_command, return_code", test_cases_should_succeed)
    def test_should_succeed(self, slk_command, return_code):
        output = subprocess.run(slk_command, capture_output=True, encoding="ascii")
        assert output.returncode == return_code, "Wrong return code"
        assert not output.stdout and not output.stderr, "No output expected"
