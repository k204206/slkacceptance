#!/bin/bash

# SLK WRAPPER

# adjust version
version='3.0.35'

# call slk jar with the corrected java
exec /sw/rhel6-x64/openjdk-15.0.2/bin/java -Xmx2g -jar /sw/rhel6-x64/slk/slk-${version}/lib/slk-cli-tools-${version}.jar "$@"

