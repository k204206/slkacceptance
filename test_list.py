import pytest
import subprocess
import utils
from pathlib import Path

test_cases = [
      [
        [ "slk", "list", "/hsm/testing/prepared/list/one_file"],
        "",
        "slk list /hsm/testing/prepared/list/one_file",  
        """Filename                                  Path                                            Size   Mode        User           Group          Modified      Status      
file_01.txt                                                                                2 B   rw-r--r--   k204221        1076           26 Mar 2021               
Files: 1
""",
        "",
        0,
        "",
        "",
        0
      ],
      [
        [ "slk", "list", "/hsm/testing/prepared/list/one_file/file_01.txt"],
        "(expected to fail)",
        "slk list /hsm/testing/prepared/list/one_file/file_01.txt",  
        """Filename                                  Path                                            Size   Mode        User           Group          Modified      Status      
file_01.txt                                                                                2 B   rw-r--r--   k204221        1076           26 Mar 2021               
Files: 1
""",
        "",
        0,
        "",
        "",
        0
      ],
      [
        [ "slk", "list", "/hsm/testing/prepared/list/one_subfolder"], 
        "",
        "slk list /hsm/testing/prepared/list/one_subfolder",
        """Filename                                  Path                                            Size   Mode        User           Group          Modified      Status      
a_folder                                  /hsm/testing/prepared/list/one_subfolder               rwxrwxrwx   900            900            26 Mar 2021               
Files: 1
""",
        "",
        0,
        "",
        "",
        0
      ],
      [
        [ "slk", "list", "/hsm/testing/prepared/list/two_files"], 
        "",
        "slk list /hsm/testing/prepared/list/two_files",
        """Filename                                  Path                                            Size   Mode        User           Group          Modified      Status      
file_01.txt                                                                                2 B   rw-r--r--   k204221        1076           26 Mar 2021               
file_02.txt                                                                                2 B   rw-r--r--   k204221        1076           26 Mar 2021     
Files: 2
""",
        "",
        0,
        "",
        "",
        0
      ],
      [
        [ "slk", "list", "/hsm/testing/prepared/list/fifty_files"], 
        "",
        "slk list /hsm/testing/prepared/list/fifty_files",
        """Filename                                  Path                                            Size   Mode        User           Group          Modified      Status      
file_01.txt                                                                                3 B   rw-r--r--   k204221        1076           26 Mar 2021               
file_02.txt                                                                                3 B   rw-r--r--   k204221        1076           26 Mar 2021               
file_03.txt                                                                                3 B   rw-r--r--   k204221        1076           26 Mar 2021               
file_04.txt                                                                                3 B   rw-r--r--   k204221        1076           26 Mar 2021               
file_05.txt                                                                                3 B   rw-r--r--   k204221        1076           26 Mar 2021               
file_06.txt                                                                                3 B   rw-r--r--   k204221        1076           26 Mar 2021               
file_07.txt                                                                                3 B   rw-r--r--   k204221        1076           26 Mar 2021               
file_08.txt                                                                                3 B   rw-r--r--   k204221        1076           26 Mar 2021               
file_09.txt                                                                                3 B   rw-r--r--   k204221        1076           26 Mar 2021               
file_10.txt                                                                                3 B   rw-r--r--   k204221        1076           26 Mar 2021               
file_11.txt                                                                                3 B   rw-r--r--   k204221        1076           26 Mar 2021               
file_12.txt                                                                                3 B   rw-r--r--   k204221        1076           26 Mar 2021               
file_13.txt                                                                                3 B   rw-r--r--   k204221        1076           26 Mar 2021               
file_14.txt                                                                                3 B   rw-r--r--   k204221        1076           26 Mar 2021               
file_15.txt                                                                                3 B   rw-r--r--   k204221        1076           26 Mar 2021               
file_16.txt                                                                                3 B   rw-r--r--   k204221        1076           26 Mar 2021               
file_17.txt                                                                                3 B   rw-r--r--   k204221        1076           26 Mar 2021               
file_18.txt                                                                                3 B   rw-r--r--   k204221        1076           26 Mar 2021               
file_19.txt                                                                                3 B   rw-r--r--   k204221        1076           26 Mar 2021               
file_20.txt                                                                                3 B   rw-r--r--   k204221        1076           26 Mar 2021               
file_21.txt                                                                                3 B   rw-r--r--   k204221        1076           26 Mar 2021               
file_22.txt                                                                                3 B   rw-r--r--   k204221        1076           26 Mar 2021               
file_23.txt                                                                                3 B   rw-r--r--   k204221        1076           26 Mar 2021               
file_24.txt                                                                                3 B   rw-r--r--   k204221        1076           26 Mar 2021               
file_25.txt                                                                                3 B   rw-r--r--   k204221        1076           26 Mar 2021   
file_26.txt                                                                                3 B   rw-r--r--   k204221        1076           26 Mar 2021               
file_27.txt                                                                                3 B   rw-r--r--   k204221        1076           26 Mar 2021               
file_28.txt                                                                                3 B   rw-r--r--   k204221        1076           26 Mar 2021               
file_29.txt                                                                                3 B   rw-r--r--   k204221        1076           26 Mar 2021               
file_30.txt                                                                                3 B   rw-r--r--   k204221        1076           26 Mar 2021               
file_31.txt                                                                                3 B   rw-r--r--   k204221        1076           26 Mar 2021               
file_32.txt                                                                                3 B   rw-r--r--   k204221        1076           26 Mar 2021               
file_33.txt                                                                                3 B   rw-r--r--   k204221        1076           26 Mar 2021               
file_34.txt                                                                                3 B   rw-r--r--   k204221        1076           26 Mar 2021               
file_35.txt                                                                                3 B   rw-r--r--   k204221        1076           26 Mar 2021               
file_36.txt                                                                                3 B   rw-r--r--   k204221        1076           26 Mar 2021               
file_37.txt                                                                                3 B   rw-r--r--   k204221        1076           26 Mar 2021               
file_38.txt                                                                                3 B   rw-r--r--   k204221        1076           26 Mar 2021               
file_39.txt                                                                                3 B   rw-r--r--   k204221        1076           26 Mar 2021               
file_40.txt                                                                                3 B   rw-r--r--   k204221        1076           26 Mar 2021               
file_41.txt                                                                                3 B   rw-r--r--   k204221        1076           26 Mar 2021               
file_42.txt                                                                                3 B   rw-r--r--   k204221        1076           26 Mar 2021               
file_43.txt                                                                                3 B   rw-r--r--   k204221        1076           26 Mar 2021               
file_44.txt                                                                                3 B   rw-r--r--   k204221        1076           26 Mar 2021               
file_45.txt                                                                                3 B   rw-r--r--   k204221        1076           26 Mar 2021               
file_46.txt                                                                                3 B   rw-r--r--   k204221        1076           26 Mar 2021               
file_47.txt                                                                                3 B   rw-r--r--   k204221        1076           26 Mar 2021               
file_48.txt                                                                                3 B   rw-r--r--   k204221        1076           26 Mar 2021               
file_49.txt                                                                                3 B   rw-r--r--   k204221        1076           26 Mar 2021               
file_50.txt                                                                                3 B   rw-r--r--   k204221        1076           26 Mar 2021  
Files: 50
""",
       "",
       0,
       "",
       "",
       0
      ],
      [
        [ "slk", "list", "/hsm/testing/prepared/list/one_file_in_a_subfolder"], 
        "(expected to fail)",
        "slk list /hsm/testing/prepared/list/one_file_in_a_subfolder",
        """Filename                                  Path                                            Size   Mode        User           Group          Modified      Status      
a_folder                                  /hsm/testing/prepared/list/one_fi...older              rwxrwxrwx   900            900            26 Mar 2021               
Files: 1
""",
        "",
        0,
        "",
        "",
        0
      ],
      [
        [ "slk", "list", "-r", "/hsm/testing/prepared/list/one_file_in_a_subfolder"], 
        "(expected to fail)",
        "slk list -r /hsm/testing/prepared/list/one_file_in_a_subfolder",
        """Filename                                  Path                                            Size   Mode        User           Group          Modified      Status      
a_folder                                  /hsm/testing/prepared/list/one_fi...older              rwxrwxrwx   900            900            26 Mar 2021               
file_a.txt                                                                                 2 B   rw-r--r--   k204221        1076           26 Mar 2021               
Files: 2
""",
        "",
        0,
        "",
        "",
        0
      ],
      [
        [ "slk", "list", "/hsm/testing/prepared/list/folder_containing_a_file_with_long_name"], 
        "",
        "slk list /hsm/testing/prepared/list/folder_containing_a_file_with_long_name",
        """Filename                                  Path                                            Size   Mode        User           Group          Modified      Status      
very_very_very_very_very_very_ver...e.txt                                                 27 B   rw-r--r--   k204221        1076           01 Apr 2021               
Files: 1
""",
        "",
        0,
        """Filename                                  Path                                            Size   Mode        User           Group          Modified      Status      
very_very_very_very_very_very_very_very_very_very_very_very_very_very_very_very_very_very_very_very_very_very_very_very_very_long_file_name.txt                                                 27 B   rw-r--r--   k204221        1076           01 Apr 2021               
Files: 1
""",
        "",
        0
      ],
      [
        [ "slk", "list", "/hsm/testing/prepared/hierarchy"], 
        "",
        "slk list /hsm/testing/prepared/hierarchy",
        """Filename                                  Path                                            Size   Mode        User           Group          Modified      Status      
abcd                                      /hsm/testing/prepared/hierarchy                   rwxrwxrwx   900            900            26 Mar 2021               
efgh                                      /hsm/testing/prepared/hierarchy                   rwxrwxrwx   900            900            26 Mar 2021               
empty                                     /hsm/testing/prepared/hierarchy                   rwxrwxrwx   900            900            26 Mar 2021               
file_01.txt                                                                                5 B   rw-r--r--   k204221        1076           26 Mar 2021               
file_02.txt                                                                                5 B   rw-r--r--   k204221        1076           26 Mar 2021               
file_01.txt                                                                               10 B   rw-r--r--   k204221        1076           26 Mar 2021               
file_a1.nc                                                                               13.1K   rw-r--r--   k204221        1076           26 Mar 2021               
file_a2.nc                                                                               13.1K   rw-r--r--   k204221        1076           26 Mar 2021               
ef                                        /hsm/testing/prepared/hierarchy/efgh              rwxrwxrwx   900            900            26 Mar 2021               
gh                                        /hsm/testing/prepared/hierarchy/efgh              rwxrwxrwx   900            900            26 Mar 2021               
file_ef.nc                                                                               13.1K   rw-r--r--   k204221        1076           26 Mar 2021               
file_g.tar                                                                               10.0K   rw-r--r--   k204221        1076           26 Mar 2021               
file_h.tar                                                                               10.0K   rw-r--r--   k204221        1076           26 Mar 2021               
Files: 13
""",
        "",
        0,
        """Filename                                  Path                                            Size   Mode        User           Group          Modified      Status      
abcd                                      /hsm/testing/prepared/hierarchy                   rwxrwxrwx   900            900            26 Mar 2021               
efgh                                      /hsm/testing/prepared/hierarchy                   rwxrwxrwx   900            900            26 Mar 2021               
empty                                     /hsm/testing/prepared/hierarchy                   rwxrwxrwx   900            900            26 Mar 2021               
file_01.txt                               /hsm/testing/prepared/hierarchy             5 B   rw-r--r--   k204221        1076           26 Mar 2021               
file_02.txt                               /hsm/testing/prepared/hierarchy             5 B   rw-r--r--   k204221        1076           26 Mar 2021               
Files: 6
""",
        "",
        0
      ],
      [
        [ "slk", "list", "-r", "/hsm/testing/prepared/hierarchy"], 
        "(expected to fail)",
        "slk list -r /hsm/testing/prepared/hierarchy",
        """Filename                                  Path                                            Size   Mode        User           Group          Modified      Status      
abcd                                      /hsm/testing/prepared/hierarchy                   rwxrwxrwx   900            900            26 Mar 2021               
efgh                                      /hsm/testing/prepared/hierarchy                   rwxrwxrwx   900            900            26 Mar 2021               
empty                                     /hsm/testing/prepared/hierarchy                   rwxrwxrwx   900            900            26 Mar 2021               
file_01.txt                                                                                5 B   rw-r--r--   k204221        1076           26 Mar 2021               
file_02.txt                                                                                5 B   rw-r--r--   k204221        1076           26 Mar 2021               
file_01.txt                                                                               10 B   rw-r--r--   k204221        1076           26 Mar 2021               
file_a1.nc                                                                               13.1K   rw-r--r--   k204221        1076           26 Mar 2021               
file_a2.nc                                                                               13.1K   rw-r--r--   k204221        1076           26 Mar 2021               
ef                                        /hsm/testing/prepared/hierarchy/efgh              rwxrwxrwx   900            900            26 Mar 2021               
gh                                        /hsm/testing/prepared/hierarchy/efgh              rwxrwxrwx   900            900            26 Mar 2021               
file_ef.nc                                                                               13.1K   rw-r--r--   k204221        1076           26 Mar 2021               
file_g.tar                                                                               10.0K   rw-r--r--   k204221        1076           26 Mar 2021               
file_h.tar                                                                               10.0K   rw-r--r--   k204221        1076           26 Mar 2021               
Files: 13
""",
        "",
        0,
        """Filename                                  Path                                            Size   Mode        User           Group          Modified      Status      
abcd                                      /hsm/testing/prepared/hierarchy                   rwxrwxrwx   900            900            26 Mar 2021               
efgh                                      /hsm/testing/prepared/hierarchy                   rwxrwxrwx   900            900            26 Mar 2021               
empty                                     /hsm/testing/prepared/hierarchy                   rwxrwxrwx   900            900            26 Mar 2021               
file_01.txt                               /hsm/testing/prepared/hierarchy             5 B   rw-r--r--   k204221        1076           26 Mar 2021               
file_02.txt                               /hsm/testing/prepared/hierarchy             5 B   rw-r--r--   k204221        1076           26 Mar 2021               
file_01.txt                               /hsm/testing/prepared/hierarchy/abcd       10 B   rw-r--r--   k204221        1076           26 Mar 2021               
file_a1.nc                                /hsm/testing/prepared/hierarchy/abcd      13.1K   rw-r--r--   k204221        1076           26 Mar 2021               
file_a2.nc                                /hsm/testing/prepared/hierarchy/abcd      13.1K   rw-r--r--   k204221        1076           26 Mar 2021               
ef                                        /hsm/testing/prepared/hierarchy/efgh              rwxrwxrwx   900            900            26 Mar 2021               
gh                                        /hsm/testing/prepared/hierarchy/efgh              rwxrwxrwx   900            900            26 Mar 2021               
file_ef.nc                                /hsm/testing/prepared/hierarchy/efgh/ef   13.1K   rw-r--r--   k204221        1076           26 Mar 2021               
file_g.tar                                /hsm/testing/prepared/hierarchy/efgh/gh   10.0K   rw-r--r--   k204221        1076           26 Mar 2021               
file_h.tar                                /hsm/testing/prepared/hierarchy/efgh/gh   10.0K   rw-r--r--   k204221        1076           26 Mar 2021               
Files: 13
""",
        "",
        0
      ],
      [
        [ "slk", "list", "/hsm/testing/prepared/list/empty"], 
        "",
        "slk list /hsm/testing/prepared/list/empty",
        "WARNING: no results returned; user may not have permission to view files or search returns no results.\n",
        "",
        0,
        "",
        "",
        0
      ],
      [
        [ "slk", "list", "/does/not/exist"], 
        "",
        "slk list /does/not/exist",
        "",
        "",
        1,
        "",
        "ERROR: cannot access /does/not/exist: No such file or directory",
        2
      ],
      [
        [ "slk", "list", "/hsm/testing/prepared/list/noreadpermission"], 
        "",
        "slk list /hsm/testing/prepared/list/noreadpermission",
        "ERROR: User does not have permission to view namespace\n",
        "",
        1,
        "",
        "ERROR: User does not have permission to view namespace",
        1
      ],
      [
        [ "slk", "list", "/hsm/testing/prepared/list/noreadpermission/havereadpermission"], # since noreadpermission has no +x, nothing inside it should be visible
        "(expected to fail)",
        "slk list /hsm/testing/prepared/list/noreadpermission/havereadpermission",
        "ERROR: User does not have permission to view namespace\n",
        "",
        1,
        "",
        "ERROR: User does not have permission to view namespace",
        1
      ]
    ]

class TestList:
    @pytest.mark.parametrize("slk_command,comment,str_cmd,expected,stderr,returncode,stdout_nice,stderr_nice,returncode_nice", test_cases)

    def test_list(self, slk_command, comment, str_cmd, expected, stderr, returncode, stdout_nice, stderr_nice, returncode_nice):
        output = subprocess.run(slk_command, capture_output=True, encoding="ascii")
        assert output.returncode == returncode
        if (output.returncode == 0 and expected):
          if "WARNING" in output.stdout: 
            assert output.stdout == expected
          else:
            utils.check_list_output(output.stdout, expected)
         
        if (stderr):
          assert output.stderr == stderr


