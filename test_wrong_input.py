import pytest
import subprocess

class TestWrongInput:
    def test_perm1(self):
        output = subprocess.run(["slk", "blablabla"], capture_output=True, encoding="ascii")
        assert output.returncode == 1

